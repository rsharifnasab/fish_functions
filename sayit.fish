function sayit
    clear
	cd ~/.config/fish/functions

    if [ (count $argv) -gt 0 ]
        python3 say_it.py "$argv"
    else
        python3 say_it.py 
    end

    cd -
end
