import time
import os
import tty
import sys
import termios
from time import sleep
import random
rnd = random.randint

def sleep_print(s, t=8):
    word = ""
    for i in s:
        word = word + i
        print(word, "\r", end="")
        sleep(rnd(50*t, 100*t)/1000)
    print()


try:
    output = " ".join(sys.argv[1:])
    sleep_print(output,4)
except KeyboardInterrupt:
    pass
