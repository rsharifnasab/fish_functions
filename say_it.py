import time
import os
import tty
import sys
import termios
from time import sleep
import random
rnd = random.randint

def sleep_print(s, t=8):
    word = ""
    for i in s:
        word = word + i
        print(word, "\r", end="")
        if i not in [ " " , "\n" ] : sleep(rnd(50*t, 100*t)/1000)
    print()

if len(sys.argv) == 2 : address = sys.argv[1]
else : address = input("welcome to this program\nplease enter address of your file :\n")
try:
    filee = open(address, "r")
    output = filee.read();
except:
    print("error in reading file")
    exit()
try:
    output =  "\n" * 20 + output
    os.system("clear")
    output = output;
    delay = 3
    if len(output) > 100: delay = 2;
    if len(output) > 1000: delay = 1;
    sleep_print(output,delay)
except KeyboardInterrupt:
    pass
