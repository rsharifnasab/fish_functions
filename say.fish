function say
    clear
	cd ~/.config/fish/functions

    if [ (count $argv) -gt 0 ]
        python3 say.py "$argv"	
    else
        python3 say.py "I Love Terminal"
    end

    cd -
end
